#!/bin/bash

cd /root/gen4/gen4/ || exit 13
ansible-playbook deploy_gen4.yml -i beta_inventory.ini -l "$1" --diff

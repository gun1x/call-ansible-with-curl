package main

import (
	"crypto/subtle"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"
)

var addServer chan string
var serverDone chan string
var token []byte

func callAnsibleScript(serverName string) {

	cmd := exec.Command("/root/call-ansible-with-curl/deploy_node.sh", serverName)

	// open the out file for writing
	outfile, err := os.Create("/var/log/ansible/" + serverName + time.Now().Format("_20060102_150405") + ".log")
	if err != nil {
		panic(err)
	}
	defer outfile.Close()
	cmd.Stdout = outfile

	err = cmd.Start()
	if err != nil {
		panic(err)
	}
	cmd.Wait()

	log.Println("DONE ", serverName)
	serverDone <- serverName
}

func queueManager() {
	var servers []string

	for {
		select {
		case serverName := <-addServer:
			exists := false
			for _, v := range servers {
				if v == serverName {
					exists = true
					break
				}
			}
			if !exists {
				log.Println("STARTING ", serverName)
				servers = append(servers, serverName)
				go callAnsibleScript(serverName)
			}
		case serverName := <-serverDone:
			for i, v := range servers {
				if v == serverName {
					servers[i] = servers[len(servers)-1]
					servers = servers[:len(servers)-1]
					break
				}
			}
		}
	}

}

func addServerToQueue(w http.ResponseWriter, r *http.Request) {
	authBool := subtle.ConstantTimeCompare([]byte(r.URL.Query().Get("token")), token)
	if authBool == 1 {
		addServer <- r.URL.Query().Get("serverName")
		w.Write([]byte("request sent to queue ;-)"))
	} else {
		http.Error(w, "auth failed", http.StatusUnauthorized)
	}
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello"))
}

func main() {
	token = []byte(os.Getenv("ADMIN_TOKEN"))
	addServer = make(chan string, 50)
	serverDone = make(chan string, 50)
	go queueManager()

	http.HandleFunc("/", sayHello)
	http.HandleFunc("/addServerToQueue", addServerToQueue)
	http.ListenAndServe("0.0.0.0:31337", nil)

}
